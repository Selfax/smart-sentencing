package de.uk.smartsentencing.springtest;

import java.io.IOException;
import java.security.Timestamp;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import de.uk.smartsentencing.elastic_operations.PrintTimestamp;
import de.uk.smartsentencing.elastic_operations.Suchanfrage;

@Controller
/**
 * Mappt die Anfrage auf http://localhost:8080/index zur HTML Seite im templates
 * Ordner und gibt diese aus.
 * 
 * @author Tim
 *
 */
public class WebController {

	PrintTimestamp tmstmp = new PrintTimestamp();
	
	/**
	 * Index als Website aufbauen und mit Java-Klasse verbinden.
	 * @param model Modell der Website.
	 * @return Gibt HTML Index-Seite zurück.
	 */
	@GetMapping("/index")
	public String formIndex(Model model) {
		model.addAttribute("index", new Index());
		return "index";
	}

	/**
	 * Bei Drücken des "Suchen" Buttons logge die aktuelle Zeit und stelle eine Suchanfrage an den Elasticsearch-Index (suche.suchanfrage()). 
	 * Anschließend formattiere die Antwort von Elastic und gebe sie aus. 
	 * @param index Index-Modell
	 * @return HTML-Seite mit Ergebnis von Elastic-Suche nach den im Formular gegebenen Parametern.
	 * @throws IOException Von Suchanfrage
	 */
	@PostMapping("/index")
    public String formSubmit(@ModelAttribute Index index) throws IOException {
    	System.out.println("====\n-->Neue Suchanfrage um " + tmstmp.getTimestamp() + "\n");
    	
    	Suchanfrage suche = new Suchanfrage(index);
    	String result = suche.suchanfrage().toString();
    	
    	System.out.println("\nInput von Webcontroller: " + result);
    	ResponseFormatter formatter = new ResponseFormatter(result);
    	System.out.println("Output aus Formatter: ");
    	formatter.format(index);
    	
        return "resultIndex";
    }

}
