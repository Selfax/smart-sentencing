package de.uk.smartsentencing.springtest;


import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class ResponseFormatter {

	String input, output;
	
	/**
	 * Lesbarmachen der JSON-Rückgabe von Elastic.
	 * @param input JSON aus Elastic Abfrage.
	 */
	public ResponseFormatter(String input) {
		this.input = input;
	}

	/**
	 * Trennt JSON Rückgabe von Elastic in Strings auf, die die Kerninformationen eines Urteils und wenige Elastic-Metadaten beinhalten.
	 * @param index Index-Objekt mit Werten aus Suchanfrage.
	 */
	public void format(Index index) {
		
		List<String> resultList = new ArrayList<String>();
		
		JSONObject jsonObject = new JSONObject(input);
		JSONObject jsonObject2 = new JSONObject(jsonObject.get("hits").toString());
		JSONArray jsonArray = new JSONArray(jsonObject2.get("hits").toString());
		
		for (Object e: jsonArray) {
			JSONObject source = (JSONObject) ((JSONObject) e).get("_source");
			System.out.println(source.get("gericht").toString() + ", " + source.get("aktenzeichen").toString() + ":");
			System.out.println("--> " + source.toString().replaceAll("\"", " ") + "\n");
			resultList.add(source.toString().replaceAll("\"", " "));
			index.setResult(source.toString().replaceAll("\"", " "));
		}
		
		if (resultList.isEmpty())
			resultList.add("Keine Ergebnisse gefunden.");
		
		index.setResultList(resultList);
		System.out.println("Results:\n" + resultList);
		
	}
}
