package de.uk.smartsentencing.springtest;

import java.util.ArrayList;
import java.util.List;

public class Index {

	/**
	 * Java-Klasse als Schnittstelle zwischen HTML-Seite und Java.
	 * @author Tim
	 */
	
	private String tatbestand;

	private String aktenzeichen;
	
	private String schadenshoeheMod;
	private float schadenshoehe;

	private String strafmassMod;
	private String strafmassModNumeric;
	private int strafmass;

	private boolean gestaendnis;
	private boolean gestaendnisBeide;
	private boolean vorstrafen;
	private boolean vorstrafenBeide;
	
	private List<String> resultList = new ArrayList<String>();
	
	public List<String> getResultList() {
		return resultList;
	}

	public void setResultList(List<String> resultList) {
		this.resultList = resultList;
	}

	private String result;


	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getTatbestand() {
		return tatbestand;
	}
	
	public void setTatbestand(String tatbestand) {
		this.tatbestand = tatbestand;
	}

	public float getSchadenshoehe() {
		return schadenshoehe;
	}
	
	public void setSchadenshoehe(float schadenshoehe) {
		this.schadenshoehe = schadenshoehe;
	}

	public String getSchadenshoeheMod() {
		return schadenshoeheMod;
	}

	public void setSchadenshoeheMod(String schadenshoeheMod) {
		this.schadenshoeheMod = schadenshoeheMod;
	}

	public int getStrafmass() {
		return strafmass;
	}

	public void setStrafmass(int strafmass) {
		this.strafmass = strafmass;
	}

	public String getStrafmassModNumeric() {
		return strafmassModNumeric;
	}

	public void setStrafmassModNumeric(String strafmassModNumeric) {
		this.strafmassModNumeric = strafmassModNumeric;
	}

	public String getStrafmassMod() {
		return strafmassMod;
	}

	public void setStrafmassMod(String strafmassMod) {
		this.strafmassMod = strafmassMod;
	}

	public boolean isGestaendnis() {
		return gestaendnis;
	}

	public void setGestaendnis(boolean gestaendnis) {
		this.gestaendnis = gestaendnis;
	}

	public boolean isVorstrafen() {
		return vorstrafen;
	}

	public void setVorstrafen(boolean vorstrafen) {
		this.vorstrafen = vorstrafen;
	}

	public boolean isGestaendnisBeide() {
		return gestaendnisBeide;
	}

	public void setGestaendnisBeide(boolean gestaendnisBeide) {
		this.gestaendnisBeide = gestaendnisBeide;
	}

	public boolean isVorstrafenBeide() {
		return vorstrafenBeide;
	}

	public void setVorstrafenBeide(boolean vorstrafenBeide) {
		this.vorstrafenBeide = vorstrafenBeide;
	}

	public String getAktenzeichen() {
		return aktenzeichen;
	}

	public void setAktenzeichen(String aktenzeichen) {
		this.aktenzeichen = aktenzeichen;
	}
	
	

}
