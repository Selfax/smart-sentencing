package de.uk.smartsentencing.springtest;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {
																																																																																																																																																																																																																																																																																	
    static public void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /**
     * Startet Spring-Server für Webentwicklung auf localhost:8080(/index).
     * @param ctx Spring Interface
     * @return Loggt "Server gestartet" sobald Server läuft.
     */
    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {

        	System.out.println("Server gestartet.");
        };
    }

}
