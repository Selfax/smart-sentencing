package de.uk.smartsentencing.elastic_operations;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PrintTimestamp {

	/**
	 * Gibt aktuelle Zeit zurück.
	 * @return Datum menschenlesbar formatiert
	 */
	public String getTimestamp() {
		SimpleDateFormat formatter= new SimpleDateFormat("HH:mm:ss");
		Date date = new Date(System.currentTimeMillis());
		
		return formatter.format(date);
	} 
}
