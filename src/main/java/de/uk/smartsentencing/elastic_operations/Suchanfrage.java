package de.uk.smartsentencing.elastic_operations;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.queryparser.xml.builders.BooleanQueryBuilder;
import org.elasticsearch.action.search.MultiSearchRequest;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import de.uk.smartsentencing.springtest.Index;

/**
 * Klasse zum Modellieren und Durchführen der Suchanfrage auf der
 * Elastic-Datenbank.
 * 
 * @author Tim
 */

public class Suchanfrage extends Connection {

	Index index = new Index();

	public Suchanfrage(Index index) {
		this.index = index;
	}

	/**
	 * Leerer Konstruktor für Testzwecke.
	 */
	public Suchanfrage() {
	}

	/**
	 * Erstellt neue Suchanfrage an Elastic und erzeugt für ausgefüllte
	 * Formularfelder eine Query, die das Matchen der Felder obligatorisch macht.
	 * 
	 * @return Antwort von Elastic
	 * @throws IOException Verbindungsparameter zu Elastic
	 */
	public SearchResponse suchanfrage() throws IOException {

		Connection.makeConnection();

		System.out.println("Suchanfrage: " + index.getTatbestand());
		SearchRequest searchRequest = new SearchRequest(INDEX);
		SearchResponse searchResponse = null;

		MultiSearchRequest request = new MultiSearchRequest();
		BoolQueryBuilder boolQuery = new BoolQueryBuilder();
		SearchSourceBuilder searchSourceBuilderTest = new SearchSourceBuilder();

		if (!index.getAktenzeichen().isEmpty()) {
			searchSourceBuilderTest
					.query(boolQuery.must(QueryBuilders.matchQuery("aktenzeichen", index.getAktenzeichen())));
		} else {
			if (index.getSchadenshoehe() != -1)
				searchSourceBuilderTest
						.query(boolQuery.must(QueryBuilders.matchQuery("schadenshoehe", index.getSchadenshoehe())));

			if (index.getStrafmass() != -1)
				searchSourceBuilderTest.query(boolQuery.must(QueryBuilders.matchQuery("tagessaetze", 0)));

			if (!index.isGestaendnisBeide())
				searchSourceBuilderTest
						.query(boolQuery.must(QueryBuilders.matchQuery("gestaendnis", index.isGestaendnis())));

			if (!index.isVorstrafenBeide())
				searchSourceBuilderTest
						.query(boolQuery.must(QueryBuilders.matchQuery("vorstrafen", index.isVorstrafen())));
		}

		searchRequest.source(searchSourceBuilderTest);

		try {
			System.out.println("Query: " + searchRequest.source().toString()); // abrufen
			searchResponse = restHighLevelClient.search(searchRequest, COMMON_OPTIONS);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Connection.closeConnection();
		return searchResponse;

	}

}
