package de.uk.smartsentencing.elastic_operations;

import java.io.IOException;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Connection {

	/**
	 * Standard Methoden und Variablen um mit Elastic auf Localhost zu entwickeln.
	 */
	
	//---Verbindungsvariablen---
		static final String HOST = "localhost";
		static final int PORT_ONE = 9200;
		static final int PORT_TWO = 9201;
		static final String SCHEME = "http";

		static final String INDEX = "urteile";

		static RestHighLevelClient restHighLevelClient;
		static ObjectMapper objectMapper = new ObjectMapper();
		
		// Request Optionen
		static final RequestOptions COMMON_OPTIONS;
		static {
			RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();
			builder.addHeader("Authorization", "Bearer ");
			COMMON_OPTIONS = builder.build();
		}

		// ---Verbindung---//

		// Verbindung herstellen
		public static synchronized RestHighLevelClient makeConnection() {

			if (restHighLevelClient == null) {
				restHighLevelClient = new RestHighLevelClient(
						RestClient.builder(new HttpHost(HOST, PORT_ONE, SCHEME), new HttpHost(HOST, PORT_TWO, SCHEME)));
			}

			return restHighLevelClient;
		}

		// Verbindung schliessen
		public static synchronized void closeConnection() throws IOException {
			restHighLevelClient.close();
			restHighLevelClient = null;
		}

}
