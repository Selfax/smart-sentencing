package de.uk.smartsentencing.elastic_operations;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import de.uk.smartsentencing.urteil.Urteil;

public class Elastic_Operations extends Connection {

	/**
	 * Erzeugt eine Map und belegt die Felder mit den Werten aus dem Urteil. Die Map
	 * wird dann auf den Index gepusht. Das Aktenzeichen wird als ID festgelegt.
	 * 
	 * @param urteil Urteilsklasse (mit Werten aus der JSON Datei)
	 */
	public static void urteilEinfuegen(Urteil urteil) {
		Map<String, Object> dataMap = new HashMap<String, Object>(); // Mapping erstellen

		// ---Eigenschaften der Instanz auf Hashmap abbilden---

		dataMap.put("aktenzeichen", urteil.getAktenzeichen());
		dataMap.put("datum", urteil.getDatum());
		dataMap.put("gericht", urteil.getGericht());
		dataMap.put("schadenshoehe", urteil.getSchadenshoehe());
		dataMap.put("monate", urteil.getStrafmass_monate());
		dataMap.put("tagessaetze", urteil.getStrafmass_tagessaetze());
		dataMap.put("bewaehrung", urteil.isBewaehrung());
		dataMap.put("gestaendnis", urteil.isGestaendnis());
		dataMap.put("vorstrafen", urteil.isVorstrafen());

		IndexRequest indexRequest = new IndexRequest(INDEX).source(dataMap);

		try {
			IndexResponse response = restHighLevelClient.index(
					indexRequest.id(urteil.getAktenzeichen() + "€" + urteil.getSchadenshoehe()), // Aktenzeichen als ID
																									// setzen
					COMMON_OPTIONS);
		} catch (ElasticsearchException e) {
			e.getDetailedMessage();
		} catch (java.io.IOException e) {
			e.getLocalizedMessage();
		}
	}

	/**
	 * (Ungenutzt) Liest Urteil aus Index aus.
	 * 
	 * @param aktenzeichen Zu findendes Urteil
	 * @return Urteil
	 */
	public static Urteil urteilAuslesen(String aktenzeichen) {

		System.out.println("getting from: " + INDEX);
		GetRequest getUrteil = new GetRequest(INDEX, aktenzeichen);
		System.out.println("with id: " + getUrteil.id()); // ID ist Aktenzeichen
		GetResponse getResponse = null;
		try {
			System.out.println("getting: " + getUrteil.toString());
			getResponse = restHighLevelClient.get(getUrteil, COMMON_OPTIONS);
		} catch (java.io.IOException e) {
			e.getLocalizedMessage();
		}
		System.out.println(getResponse.toString());
		return getResponse != null ? objectMapper.convertValue(getResponse.getSourceAsMap(), Urteil.class) : null;
	}

	/**
	 * (Ungenutzt) Sucht im Index nach Urteil mit genanntem Aktenzeichen.
	 * 
	 * @param aktenzeichen Zu findendes Aktenzeichen
	 */
	private static void sucheAktenzeichen(String aktenzeichen) {
		System.out.println("with id: " + aktenzeichen);
		SearchRequest searchRequest = new SearchRequest(INDEX);
		SearchResponse searchResponse = null;

		// in Source des Dokuments suchen, Query definieren
		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

		String[] includeFields = new String[] { "aktenzeichen" };
		String[] excludeFields = new String[] {};
		searchSourceBuilder.fetchSource(includeFields, excludeFields);

		MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("aktenzeichen", aktenzeichen);

		// In Feld Aktenzeichen nach übergebenem Aktenzeichen suchen
		searchRequest.source(searchSourceBuilder.query(matchQueryBuilder)); // SourceBuilder auf SearchRequest anwenden
																			// query auf SourceBuilder anwenden

		System.out.println(searchRequest.source().toString());

		try {
			searchResponse = restHighLevelClient.search(searchRequest, COMMON_OPTIONS); // searchResponse mit Request
																						// abrufen
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Found: " + searchResponse.toString());
	}

}
