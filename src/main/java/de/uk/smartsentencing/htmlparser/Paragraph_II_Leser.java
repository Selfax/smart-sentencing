package de.uk.smartsentencing.htmlparser;

import java.util.Scanner;

//Diese Klasse unterscheidet die Anzahl der Einzeltaten und parst die Informationen aus Absatz II.
//TODO Schadenshöhe und Datum in Urteil einpflegen
	// + Einzeltaten anhand Absatz V. abgleichen!

public class Paragraph_II_Leser extends Paragraph_II {

	String html;
	
	public Paragraph_II_Leser(String html) {
		this.html = html;
		parse();
	}

	public void parse() {
		//Absatz II. aus Paragraphen lesen
		paragraphIIFinden(html);
		
	}

	/**
	 * Sucht im HTML-Volltext nach Absatz II. und übergibt gesamten Absatz zur
	 * weiteren Verarbeitung.
	 * 
	 * @param html HTML-Volltext aus Webzugriff
	 */
	public void paragraphIIFinden(String html) {

		Scanner scanner = new Scanner(html.toLowerCase());
		String paragraph = null;

		while (scanner.hasNextLine()) {
			try {
				if (scanner.nextLine().contains("ii.")) {
					paragraph = (String) html.subSequence(html.indexOf("II."), html.lastIndexOf("III."));
				}
			} catch (StringIndexOutOfBoundsException e) {
				paragraph = (String) html.subSequence(html.indexOf("II."), html.indexOf("III."));
			}
		}
		scanner.close();
		
		// Slots des Array auf die Anzahl der Taten anpassen
		tatenArray = paragaphIIParsen(paragraph);

		String[] temp = new String[anzahlTaten];
		System.arraycopy(tatenArray, 0, temp, 0, anzahlTaten);
		tatenArray = temp;

	}

	/**
	 * Sucht im übergebenen Paragraphen Zeile für Zeile nach Regex-Muster um
	 * Tagesdaten und Eurozeichen zu finden. Anhand der Daten können Absätze
	 * unterschieden werden. Die gefundene Zeile wird ins tatenArray gespeichert.
	 * 
	 * @param paragraph Absatz II des HTML-Urteils
	 * @return Array mit den Textzeilen, in denen Tagesdaten gefunden wurden
	 */
	public String[] paragaphIIParsen(String paragraph) {

		Scanner absatzLeser = new Scanner(paragraph.toLowerCase()).useDelimiter("/>");

		
		while (absatzLeser.hasNext()) {
			String zeile = absatzLeser.nextLine();
			//Regex für: (TT.MM.JJJJ ODER TT. [Monat] JJJJ) + Eurozeichen
			//Matcht also: 27.07.2019 oder (am) 27. (des) Juli(s) 2019
			if ((zeile.matches(".*\\d{2}\\S\\d{2}\\S\\d{4}.*") || zeile.matches(".*\\d{2}.*\\w{3,9}.*\\d{4}.*"))
					&& zeile.contains("€") || zeile.contains("&#8364") || zeile.toLowerCase().contains("euro")) {
				System.out.println("-->Suche nach Einzeltat..."); // TEST
				System.out.println("-->Found: " + zeile); // TEST
				tatenArray[anzahlTaten] = zeile;
				anzahlTaten++;
			}
		} 
		absatzLeser.close();
		return tatenArray;
	}

	public int getAnzahlTaten() {
		return anzahlTaten;
	}
	
	public void setAnzahlTaten(int anzahlTaten) {
		this.anzahlTaten = anzahlTaten;
	}
	
	public String[] getTatenArray() {
		return tatenArray;
	}

	public void setTatenArray(String[] tatenArray) {
		this.tatenArray = tatenArray;
	}

	public float getSchadenshoehe() {
		return schadenshoehe;
	}

	public void setSchadenshoehe(float schadenshoehe) {
		this.schadenshoehe = schadenshoehe;
	}


}
