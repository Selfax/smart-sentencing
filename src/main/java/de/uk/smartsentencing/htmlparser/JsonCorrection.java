package de.uk.smartsentencing.htmlparser;

import java.util.Scanner;

import org.json.JSONObject;

import com.google.gson.JsonObject;

/**
 * Korrektur der Auslese, falls die Daten nicht mit dem Urteil übereinstimmen.
 * @author Tim
 *
 */
public class JsonCorrection  {

	private String fileName;
	public JSONObject json;
	private JSONObject backUpJson;

	public JsonCorrection(String fileName, JSONObject json) {
		this.fileName = fileName;
		this.json = json;
		backUpJson = new JSONObject(json, JSONObject.getNames(json));
		
		System.out.println("Korrektur von " + fileName + " gestartet.");
		landingMenu();
	}

	/**
	 * Menü der Korrektur. Läuft solange, wie nicht beendet oder abgebrochen wird.
	 */
	private void landingMenu() {

		Scanner menu = new Scanner(System.in);
		String menuInput = null;

		while (menuInput == null || !menuInput.matches("a|d|s|g|t|m|ge|v|e|c")) {
			while (menuInput == null || menuInput.matches("a|d|s|g|t|m|ge|v")) {
			menuInput = null;
			System.out.println(
					"\n---Korrektur---\n" + "\nJson: " + json.toString() + "\n" + "\nWas soll geändert werden?");

			System.out.println("Wert auswählen:\n" 
					+ "(a) Aktenzeichen\n" 
					+ "(d) Datum\n" 
					+ "(g) Gericht\n"
					+ "(s) Schadenshöhe\n"
					+ "(t) Tagessätze\n" 
					+ "(m) Monate\n" 
					+ "(ge) Geständnis\n" 
					+ "(v) Vorstrafen\n"
					+ "Zum Beenden: (e) EXIT (speichere Änderungen)\n" 
					+ "Zum Abbrechen: (c) CANCEL (Änderungen gehen verloren)\n"
					+ "Eingabe: "
					);
			menuInput = menu.nextLine().toLowerCase();
			changeJson(menuInput);
		}
			if (menuInput.matches("e|c")) {	
				break;
			} else {
				System.out.println("Keine korrekte Eingabe.");
				menuInput=null;
				continue;
			}
		}
	}

	/**
	 * Ändert das Attribut der JSON-Datei, das eingegeben wurde.
	 * @param menuInput
	 */
	private void changeJson(String menuInput) {

		Scanner change = new Scanner(System.in);
		String changeInput = null;

		switch (menuInput) {
		case "a":
			System.out.println("Aktenzeichen");
			System.out.println("Neues Aktenzeichen eingeben (Achtung! Keinen Schrägstrich '/' verwenden!):");
			changeInput = change.nextLine().toLowerCase();
			if (changeInput.isEmpty()) {
				System.out.println("Keine Änderung.");
			} else {
				json.put("aktenzeichen", changeInput);
			}
			System.out.println("Neuer Wert: " + changeInput);
			System.out.println("Kehre zu Menü zurück.");
			break;

		case "d":
			System.out.println("Datum");
			System.out.println("Neues Datum eingeben (Achtung! Format DD.MM.YYYY verwenden!):");
			changeInput = change.nextLine().toLowerCase();
			if (changeInput.isEmpty()) {
				System.out.println("Keine Änderung.");
			} else {
				json.put("datum", changeInput);
				System.out.println("Neuer Wert: " + changeInput);
			}
			System.out.println("Kehre zu Menü zurück.");
			break;
			
		case "g":
			System.out.println("Gericht");
			System.out.println("Neues Gericht eingeben (Achtung! Abkürzungen (AG, LG, VG, etc.) ausschreiben!):");
			changeInput = change.nextLine().toLowerCase();
			if (changeInput.isEmpty()) {
				System.out.println("Keine Änderung.");
			} else {
				json.put("gericht", changeInput);
				System.out.println("Neuer Wert: " + changeInput);
			}
			System.out.println("Kehre zu Menü zurück.");
			break;
			
		case "s":
			System.out.println("Schadenshöhe");
			System.out.println("Neue Schadenshöhe eingeben (Achtung! Nur positive Kommazahlen oder 0.0, jeweils mit '.' statt ',', verwenden!):");
			changeInput = change.nextLine().toLowerCase();
			if (changeInput.isEmpty()) {
				System.out.println("Keine Änderung.");
			} else {
				if (changeInput.matches("-?\\d+")) {
					changeInput.replaceAll(",", ".");
					json.put("schadenshöhe", Float.parseFloat(changeInput));
					System.out.println("Neuer Wert: " + changeInput);
				} else {
					System.out.println("Es wurde keine Zahl eingegeben. Keine Änderung.");
				}
			}
			System.out.println("Kehre zu Menü zurück.");
			break;
			
		case "t":
			System.out.println("Tagessätze");
			System.out.println("Neue Tagessätze eingeben (Achtung! Nur positive ganze Zahlen oder 0 verwenden!):");
			changeInput = change.nextLine().toLowerCase();
			if (changeInput.isEmpty()) {
				System.out.println("Keine Änderung.");
			} else {
				if (changeInput.matches("-?\\d+")) {
					json.put("tagessätze", Integer.parseInt(changeInput));
					System.out.println("Neuer Wert: " + changeInput);
				} else {
					System.out.println("Es wurde keine Zahl eingegeben. Keine Änderung.");
				}
			}
			System.out.println("Kehre zu Menü zurück.");
			break;
			
		case "m":
			System.out.println("Monate");
			System.out.println("Neue Monate eingeben (Achtung! Nur positive ganze Zahlen oder 0 verwenden!):");
			changeInput = change.nextLine().toLowerCase();
			if (changeInput.isEmpty()) {
				System.out.println("Keine Änderung.");
			} else {
				if (changeInput.matches("-?\\d+")) {
					json.put("monate", Integer.parseInt(changeInput));
					System.out.println("Neuer Wert: " + changeInput);
				} else {
					System.out.println("Es wurde keine Zahl eingegeben. Keine Änderung.");
				}
			}
			System.out.println("Kehre zu Menü zurück.");
			break;
			
		case "ge":
			System.out.println("Geständnis");
			System.out.println("Eingeben, ob ein Geständnis vorhanden ist (Achtung! Nur True oder False verwenden!):");
			changeInput = change.nextLine().toLowerCase();
			if (changeInput.isEmpty()) {
				System.out.println("Keine Änderung.");
			} else {
				if (changeInput.toLowerCase().matches("true|t")) {
					json.put("geständnis", true);
					System.out.println("Neuer Wert: true");
				} else if (changeInput.toLowerCase().matches("false|f")) {
					json.put("geständnis", false);
					System.out.println("Neuer Wert: false");
				} else {
					System.out.println("Es wurde kein Boolescher Wert eingegeben. Keine Änderung.");
				}
			}
			System.out.println("Kehre zu Menü zurück.");
			break;
			
		case "v":
			System.out.println("Vorstrafen");
			System.out.println("Eingeben, ob Vorstrafen vorhanden sind (Achtung! Nur True oder False verwenden!):");
			changeInput = change.nextLine().toLowerCase();
			if (changeInput.isEmpty()) {
				System.out.println("Keine Änderung.");
			} else {
				if (changeInput.toLowerCase().matches("true|t")) {
					json.put("vorstrafen", true);
					System.out.println("Neuer Wert: true");
				} else if (changeInput.toLowerCase().matches("false|f")) {
					json.put("vorstrafen", false);
					System.out.println("Neuer Wert: false");
				} else {
					System.out.println("Es wurde kein Boolescher Wert eingegeben. Keine Änderung.");
				}
			}
			System.out.println("Kehre zu Menü zurück.");
			break;
			
		case "e":
			System.out.println("EXIT");
			System.out.println("*Beende Korrektur.*\n");
			break;
			
		case "c":
			System.out.println("CANCEL");
			System.out.println("Mache Änderungen rückgängig.");
			System.out.println("*Beende Korrektur.*\n");
			json = backUpJson;
			break;
		}
	}

}
