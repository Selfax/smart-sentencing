package de.uk.smartsentencing.htmlparser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import de.uk.smartsentencing.urteil.Urteil;

public class HtmlToJson extends Paragraph_II {

	
	public HtmlToJson(String urteilUrl) {
		this.urteilUrl = urteilUrl;
	}
	
	String fileName;

	List<Urteil> urteilListe = new ArrayList<Urteil>();

	boolean einzeltat; // Zur Bestimmung, ob Urteil mehrere Einzeltaten enthält

	String aktenzeichen;
	String gericht;
	int strafmass_monate;
	int strafmass_tagessaetze;
	int schadenshoehe;
	boolean bewaehrung;
	boolean gestaendnis;
	boolean vorstrafen;
	String urteilUrl;
	
	/**
	 * Lädt Text aus der übergebenen URL und durchsucht ihn nach relevanten Werten.
	 * 
	 * @param url URL des Urteils
	 * @throws Exception IOException (HTMLToString) und JSONException (toJSON)
	 */
	public void webZugriff(String url) throws Exception {

		String html = htmlToString(url);

		// Unterscheidung zwischen Einzeltaten
		Paragraph_II_Leser paragraphenLeser = new Paragraph_II_Leser(html);
		tatenArray = paragraphenLeser.getTatenArray();
		anzahlTaten = paragraphenLeser.getAnzahlTaten();

		// ----Für jede Tat aus tatenArray: alle Attribute auslesen und zuweisen----//
		System.out.println();
		System.out.println("=>Schadenshöhen auslesen und zuordnen");
		
		erzeugeUrteile(html, paragraphenLeser);

		// ---Zu JSON parsen---
		System.out.println();
		System.out.println("=>Dateien erstellen (Ordner 'JSON_Urteile': '/' wird durch '_' ersetzt)");
		
		JsonToDirectory jsonToDirectory = new JsonToDirectory();
		
		int t = 1;
		for (Urteil e : urteilListe) {
			System.out.println("\n" + t + ". Urteil" + "\n");
			t++;
			urteilToJson(e, jsonToDirectory);
		}
	}

	/**
	 * Erzeugt für jede Zeile, die im Tatenarray gespeichert ist, ein neues Urteil und setzt die neuen Objektwerte entsprechend den Informationen
	 * aus der Urteilszeile.
	 * @param html HTML-Volltext
	 * @param abl Instanz des Paragraph_II_Lesers
	 */
	private void erzeugeUrteile(String html, Paragraph_II_Leser abl) {


		int t = 1;

		for (String e : tatenArray) {

			System.out.println("\n" + t + ". Urteil" + "\n");
			t++;
			
			Urteil urteil = new Urteil();

			HtmlParser parser = new HtmlParser();

			String title = parser.getTitle(html);
			urteil.setAktenzeichen(parser.getAktenzeichen(title));
			urteil.setGericht(parser.getGericht(title));
			urteil.setDatum(parser.getDatum(e));
			
			urteil.setSchadenshoehe(parser.getSchadenshoehe(e));
			int temp = parser.getStrafmass(html);
			if (temp != 0 && parser.tagessaetze) {
				urteil.setStrafmass_tagessaetze(temp);
				urteil.setStrafmass_monate(0);
			} else {
				urteil.setStrafmass_tagessaetze(0);
				urteil.setStrafmass_monate(temp);
			}

			urteil.setGestaendnis(parser.getGestaendnis(html));
			urteil.setVorstrafen(parser.getVorstrafen(html));
			urteil.setUrl(urteilUrl);

			urteilListe.add(urteil);
		}
		
		System.out.println("===========");
		System.out.println("===========");
		
	}

	/**
	 * Der Methode wird ein URL-String übergeben, die Seite mit einem Reader
	 * geöffnet und Zeichen für Zeichen mittels Stringbuilder ausgelesen.
	 * Zurückgegeben wird der HTML-Volltext mit Tags. Zusätzlich wird eine Temporäre
	 * Datei mit dem Volltext erstellt.
	 * 
	 * @param url URL des Urteils
	 * @return Volltext des HTML-Urteils
	 * @throws IOException Input-Output-Reader
	 */
	public String htmlToString(String url) throws IOException {

		StringBuilder stringBuilder = new StringBuilder();

		try {
			URL methodUrl = new URL(url);

			// Stream zur HTML-Seite öffnen
			BufferedReader in = new BufferedReader(new InputStreamReader(methodUrl.openStream()));

			int c = 0;

			while ((c = in.read()) != -1) {
				stringBuilder.append((char) c);
			}
			in.close();
		} catch (MalformedURLException e) {
			System.out.println("Malformed URL: " + e.getMessage());
		}

		htmlToFile("Temp.txt", stringBuilder.toString());

		return stringBuilder.toString();
	}

	/**
	 * Gibt HTML-Volltext in temporäre Datei Temp.txt aus.
	 * 
	 * @param fileName  Dateinamen festlegen
	 * @param inputText Wird in Datei geschrieben (hier: HTML-Volltext)
	 * @throws IOException Writer
	 */
	public void htmlToFile(String fileName, String inputText) throws IOException {

		String str = inputText + "\n";
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		writer.write(str);

		writer.close();
	}
	
	/**
	 * Zwei JSON Objekte: json (komplettes JSON) und gesamtstrafe (temporär) Die aus
	 * der HTML ausgelesenen Werte werden mit entsprechendem Key in gesamtstrafe
	 * gespeichert. Anschließend wird gesamtstrafe asl Objekt in json eingefügt.
	 * @throws IOException 
	 */
	public JSONObject urteilToJson(Urteil e, JsonToDirectory jtd) throws JSONException, IOException {

			JSONObject json = new JSONObject();
			JSONObject urteil = new JSONObject();

			urteil = mapToJson(urteil, "aktenzeichen", e.getAktenzeichen());
			urteil = mapToJson(urteil, "gericht", e.getGericht());
			urteil = mapToJson(urteil, "datum", e.getDatum());
			urteil = mapToJson(urteil, "schadenshöhe", e.getSchadenshoehe());
			urteil = mapToJson(urteil, "tagessätze", e.getStrafmass_tagessaetze());
			urteil = mapToJson(urteil, "monate", e.getStrafmass_monate());
			urteil = mapToJson(urteil, "geständnis", e.isGestaendnis());
			urteil = mapToJson(urteil, "vorstrafen", e.isVorstrafen());
			urteil = mapToJson(urteil, "url", e.getUrl());

			json.put("urteil", urteil);
			
			jtd.jsonToFile(urteil);
			
			return urteil;
	}

	/**
	 * Füge String-Objekt Paar zu JSONObjekt hinzu.
	 * 
	 * @param json   Objekt, in das key und object eingefügt werden.
	 * @param key    Name des Wertes
	 * @param object Wert
	 * @return Neues JSONObjekt
	 */
	public JSONObject mapToJson(JSONObject json, String key, Object object) {

		try {
			json.put(key, object);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return json;
	}

	// Getter
	public String getFile() {
		return fileName;
	}

}
