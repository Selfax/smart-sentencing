package de.uk.smartsentencing.htmlparser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Erstellt mit dem aus HTMLToJSON übergebenen Text eine neue JSON Datei in
 * einem festgelegten Ordner: JSON_Urteile. Der jeweilige Dateiname ist das
 * Aktenzeichen des Urteils, in dem '/' durch '_' ausgetauscht werden, gefolgt
 * von einem '€' und der aus dem JSON gelesenen Schadenshöhe.
 * 
 * @author Tim
 *
 */
public class JsonToDirectory {

	public JsonToDirectory() {
		new File("JSON_Urteile").mkdirs(); // ANPASSEN
	}

	/**
	 * Erstellt im Ordner JSON_Urteile eine JSON Datei mit dem Aktenzeichen als
	 * Namen und der Schadenshöhe hinter dem Delimiter €.
	 * 
	 * @param json Fertiges JSON aus HTMLToJSON
	 * @throws JSONException
	 * @throws IOException
	 */
	public void jsonToFile(JSONObject json) throws JSONException, IOException {
		File dir = new File("JSON_Urteile");

		String fileName = json.get("aktenzeichen").toString() + "€" + json.get("schadenshöhe").toString() + ".json";
		boolean jsonDateiVorhanden = false;

		if (fileName.contains("/"))
			fileName = fileName.replace("/", "_");

		frageNutzerNachBestaetigung(fileName, json, jsonDateiVorhanden, dir);
	}

	/**
	 * Fragt den Nutzer, ob die Datei erstellt und, falls sie bereits vorhanden ist, überschrieben werden soll.
	 * @param fileName Vorher zusammengesetzter Dateiname.
	 * @param json JSON Objekt der Datei.
	 * @param vorhanden Boolean, ob Dateiname bereits vorhanden ist.
	 * @param dir Ordner mit JSON Dateien.
	 * @throws IOException
	 */
	private void frageNutzerNachBestaetigung(String fileName, JSONObject json, boolean vorhanden, File dir)
			throws IOException {
		System.out.println("Bitte überprüfen:");
		System.out.println("--> " + fileName + ": " + json.toString()); 
		System.out.println("\n'" + fileName + "' erstellen (y/n) oder ändern (c)?");

		Scanner approval = new Scanner(System.in);
		String input = null;
		input = approval.nextLine().toLowerCase();

		while (!input.matches("y|yes|n|no|c|ändern")) {
			System.out.println();
			System.out.println("Bitte überprüfen:");
			System.out.println("--> " + fileName + ": " + json.toString()); 
			System.out.println("\n'" + fileName + "' erstellen (y/n) oder ändern (c)?");
			input = approval.nextLine().toLowerCase();
		}

		File temp = null;
		if (input.equals("yes") || input.equals("y")) {
			for (File e : dir.listFiles()) {
				if (e.getName().equals(fileName)) {
					vorhanden = true;
					temp = e;
					break;
				}
			}
			if (vorhanden) {
				dateiUeberschreiben(fileName, json, vorhanden, dir, temp);
			} else {
				dateiErstellen(fileName, json);
			}
		} else if (input.equals("no") || input.equals("n")) {
			System.out.println("Datei wurde nicht erstellt.");
		} else if (input.equals("c") || input.equals("ändern")) {
			JsonCorrection correction = new JsonCorrection(fileName, json);
			json = correction.json;
			jsonToFile(json);
		}
	}


	/**
	 * Zeigt bereits bestehende Dateien an und überschreibt diese, falls Nutzer zustimmt.
	 * @param fileName Vorher zusammengesetzter Dateiname.
	 * @param json JSON Objekt der Datei.
	 * @param vorhanden Boolean, ob Dateiname bereits vorhanden ist.
	 * @param dir Ordner mit JSON Dateien.
	 * @param showFile Bereits bestehende Datei mit selbem Namen.
	 * @throws IOException
	 */
	private void dateiUeberschreiben(String fileName, JSONObject json, boolean vorhanden, File dir, File showFile)
			throws IOException {
		Scanner approval = new Scanner(System.in);
		String secondInput = "t";

		while (!secondInput.matches("y|yes|n|no")) {
			System.out.println(
					"'" + fileName + "' existiert bereits. Überschreiben(y/n) oder existierende Datei (a)nzeigen?");
			secondInput = approval.nextLine().toLowerCase();
			if (secondInput.equals("a") || secondInput.equals("anzeigen")) {
				BufferedReader br = new BufferedReader(new FileReader(showFile));
				String st;
				boolean gleich = false;

				System.out.println("\nAlte Datei:");
				while ((st = br.readLine()) != null) {
					System.out.println(st + "\n");
					if (st.equals(json.toString()))
						gleich = true;
				}
				
				System.out.println("Neue Datei:");
				System.out.println(json.toString());

				if (gleich)
					System.out.println("(Dateien sind gleich.)\n");
			}
		}

		if (secondInput.equals("yes") || secondInput.equals("y")) {
			for (File e : dir.listFiles()) {
				if (e.getName().equals(fileName)) {
					e.delete();
					break;
				}
			}
			dateiErstellen(fileName, json);
		} else {
			System.out.println("Datei wurde nicht erstellt.");
		}
	}

	/**
	 * Erstellt eine neue Datei mit dem übergebenem Dateinamen.
	 * @param fileName Ersteller Dateiname.
	 * @param json JSON Objekt der Datei.
	 * @throws IOException
	 */
	private void dateiErstellen(String fileName, JSONObject json) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter("JSON_Urteile\\" + fileName));
		writer.write(json.toString());

		writer.close();

		System.out.println("'" + fileName + "' wurde erstellt.");
	}
}
