package de.uk.smartsentencing.htmlparser;

import java.io.Reader;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HtmlParser extends Paragraph_II {

	boolean tagessaetze;

	/**
	 * Navigiert zu Title-Tags und gibt Text zwischen den Tags zurück.
	 * 
	 * @param html HTML-Volltext
	 * @return Title ohne Tags
	 */
	public String getTitle(String html) {

		String title = null;

		html = html.replaceAll("\\s+", " ");
		Pattern p = Pattern.compile("<title>(.*?)</title>");
		Matcher m = p.matcher(html);
		while (m.find() == true) {
			title = m.group(1);
		}

		return title;
	}

	/**
	 * Nimmt mit festgelegtem Index das Aktenzeichen aus dem Titel.
	 * 
	 * @param title Titel des HTML-Urteils
	 * @return Aktenzeichen
	 */
	public String getAktenzeichen(String title) {
		return title.subSequence(title.indexOf(",") + 2, title.length() - 1).toString();
	}

	/**
	 * Liest mit festgelegtem Index das Gericht aus dem Titel.
	 * 
	 * @param title Titel des HTML-Urteils
	 * @return Gericht
	 */
	public String getGericht(String title) {
		return title.subSequence(1, title.indexOf(",")).toString();
	}

	/**
	 * Sucht in der übergebenen Zeile des Urteils nach einem Datumsformat und übergibt dieses, wenn es gefunden wird.
	 * @param e
	 * @return
	 */
	public String getDatum(String e) {
		
		//Matcht sowohl 23.04.2018 als auch 23. April 2018
		String patternStr = "\\d{2}\\S\\d{2}\\S\\d{4}|\\d{2}.(\\s)\\w{3,9}\\s\\d{4}";
		Pattern pattern = Pattern.compile(patternStr);
		Matcher matcher = pattern.matcher(e);
		
		if (matcher.find()) {
			String date = e.subSequence(matcher.start(), matcher.end()).toString();
			if (date.toLowerCase().matches("\\d{2}.(\\s)\\w{3,9}\\s\\d{4}")) {
				date = formatDate(date);
				return date;
			}
			return e.subSequence(matcher.start(), matcher.end()).toString();
		}
		
			return "nicht gefunden";
	}

	/**
	 * Erkennt Monat und gibt Datum in DD.MM.YYYY zurück.
	 * @param date Datum String aus Urteil
	 * @return Datum in neuem Format
	 */
	private String formatDate(String date) {
		
		String monthPattern = "\\w{3,9}";
		Pattern pattern = Pattern.compile(monthPattern);
		Matcher matcher = pattern.matcher(date);
		
		String month = null;
		
		if (matcher.find())
			month = matcher.group();
		
		switch (month) {
			case "januar": 
				date = date.replaceAll("\\. januar ", ".01.");
				break;
			case "februar": 
				date = date.replaceAll("\\. februar ", ".02.");
				break; 
			case "märz": 
				date = date.replaceAll("\\. märz ", ".03.");
				break; 
			case "april": 
				date = date.replaceAll("\\. april ", ".04.");
				break; 
			case "mai": 
				date = date.replaceAll("\\. mai ", ".05.");
				break; 
			case "juni": 
				date = date.replaceAll("\\. juni ", ".06.");
				break; 
			case "juli": 
				date = date.replaceAll("\\. juli ", ".07.");
				break; 
			case "august": 
				date = date.replaceAll("\\. august ", ".08.");
				break; 
			case "september": 
				date = date.replaceAll("\\. september ", ".09.");
				break; 
			case "oktober": 
				date = date.replaceAll("\\. oktober ", ".10.");
				break; 
			case "november":  
				date = date.replaceAll("\\. november ", ".11.");
				break;
			case "dezember": 
				date = date.replaceAll("\\. dezember ", ".12.");
				break;
		}	
		return date;
	}

	/**
	 * Für jede Zeile in übergebenem Array: Suche nach Match für Regex Muster
	 * (s.u.), wenn gefunden, setze Schadenshöhe auf den entsprechenden Wert.
	 * 
	 * @param array
	 * @return
	 */
	public float getSchadenshoehe(String e) {

		System.out.println("-->Schadenshöhe auslesen ..."); // TEST

		// Regex um Eurobetrag zu finden
		// Beliebige Anzahl von Ziffern (+ Komma + zwei Ziffern) (+ Whitespace) + "euro" ODER "€" ODER "&#8364"
		Pattern p = Pattern.compile("\\d*((\\S\\d{2})|(\\S{2}))?(\\s)?(euro|€|&#8364)");

		System.out.println("Zeile: " + e); // TEST
		Matcher m = p.matcher(e);
		while (m.find() == true) {
			System.out.println("Found: " + m.group(0)); // TEST

			// Kommata durch Punkte ersetzen in Dezimalzahlen
			if (!m.group(0).contains(",-")) {
				schadenshoehe = Float.parseFloat((String) ((String) m.group(0).subSequence(0, m.group(0).indexOf(" "))).replace(",", ".")); 
			} else {
				schadenshoehe = Float.parseFloat((String) ((String) m.group(0).subSequence(0, m.group(0).indexOf(","))).replace(",", ".")); 
			}
			System.out.println("-->Schadenshöhe: " + schadenshoehe); // TEST
		}

		return schadenshoehe;
	}

	/**
	 * Entscheidet mit Regex-Ausdrücken ob Tagessätze oder Haftstrafe vorliegen und
	 * liest entsprechenden Wert aus.
	 * 
	 * @param html HTML-Volltext
	 * @return Strafmass (Tagessätze oder Haftstrafe)
	 */
	public int getStrafmass(String html) {
		int strafmass_monate = 0;
		int strafmass_tagessaetze = 0;

		String strafmass_monateString = null;
		String strafmass_tagessaetzeString = null;

		Scanner scanner = new Scanner(html);

		try {
			while (scanner.hasNext() && (strafmass_tagessaetzeString == null && strafmass_monateString == null)) {

				String scannerZeile = scanner.nextLine();
				Scanner temp = new Scanner(scannerZeile);
				
				if (scannerZeile.contains("Tagess&#228;tze")) {
					strafmass_tagessaetzeString = temp.findInLine("\\d* Tagess&#228;tze");
					temp.close();
					scanner.close();
					tagessaetze = true;
					return strafmass_tagessaetze = getTagessaetze(strafmass_tagessaetzeString);
				} else if (scannerZeile.contains("Freiheitsstrafe von")) {
					strafmass_monateString = temp.findInLine("Freiheitsstrafe \\w{0,10} \\w{0,10} \\w{0,10}");
					temp.close();
					scanner.close();
					tagessaetze = false;
					return strafmass_monate = getHaftstrafe(strafmass_monateString);
				} else {
					continue;
				}
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
			System.err.println("Keine Tagessätze oder Haftstrafe gefunden.");
			return 0;
		}
		scanner.close();
		return 0;
	}

	/**
	 * Liest aus gegebenem String die Tagessätze aus und parst sie als Integer.
	 * 
	 * @param tagessaetzeString Textausschnitt
	 * @return Tagessätze als Integer
	 */
	public int getTagessaetze(String tagessaetzeString) {
		int strafmass_tagessaetze = 0;
		
		tagessaetzeString = tagessaetzeString.substring(0,3).strip();
		
		if (tagessaetzeString.length()<=2) {
			strafmass_tagessaetze = Integer.parseInt(tagessaetzeString.subSequence(0, 2).toString());
		} else {
			strafmass_tagessaetze = Integer.parseInt(tagessaetzeString.subSequence(0, 3).toString());
		}
		
		return strafmass_tagessaetze;
	}

	/**
	 * Liest aus gegebenem String die Haftstrafe aus und parst sie als Integer.
	 * 
	 * @param tagessaetzeString Textausschnitt
	 * @return Haftstrafe in Monaten als Integer
	 */
	public int getHaftstrafe(String haftstrafeString) {
		int strafmass_monate = 0;

		String[] zahlenListe = new String[4];
		zahlenListe = haftstrafeString.split(" ");

		haftstrafeString = zahlenListe[2];

		WordToIntParser wordParser = new WordToIntParser();
		strafmass_monate = wordParser.parse(haftstrafeString);

		return strafmass_monate;
	}

	/**
	 * Durchsucht HTML-String nach "geständ" und gibt true zurück, wenn es gefunden
	 * wird.
	 * 
	 * @param html Die String Repräsentation des Websiteninhalts inklusive Tags
	 * @return True wenn Schlagwort gefunden wird
	 */
	public boolean getGestaendnis(String html) {
		Scanner scanner = new Scanner(html.toLowerCase());

		while (scanner.hasNextLine()) {
			if (scanner.nextLine().contains("gest&#228;nd")) {
				return true;
			} else {
				continue;
			}
		}

		return false;
	}

	/**
	 * Durchsucht HTML nach "strafrechtlich (Puffer) noch nicht in erscheinung" -
	 * true wenn gefunden
	 */
	public boolean getVorstrafen(String html) {
		Scanner scanner = new Scanner(html.toLowerCase());
		
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			if ((line.matches(".*strafrechtlich (.*)? nicht in erscheinung.*"))||(line.contains("strafrechtlich nicht in erscheinung"))) {
				return false;
			} else {
				continue;
			}
		}

		return true;
	}
}
