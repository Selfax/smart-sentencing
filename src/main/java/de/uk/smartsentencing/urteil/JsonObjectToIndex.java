package de.uk.smartsentencing.urteil;

import java.io.File;
import java.io.IOException;

import de.uk.smartsentencing.elastic_operations.Connection;
import de.uk.smartsentencing.elastic_operations.Elastic_Operations;

public class JsonObjectToIndex {

	File folder;
	
	public JsonObjectToIndex(File folder) {
		this.folder = folder;
		dirToIndex();
	}

	/**
	 * Liest den in der Klasse Main angegebenen Ordner aus und fügt die enthaltenen JSON Dateien in den Index ein.
	 */
	private void dirToIndex() {
		int zaehler = 1;
		for (final File file : folder.listFiles()) { // für jede Datei aus dem Ordner "Urteile": Datei auslesen und indexieren
			if (!file.getName().contains("json")) {
				System.err.print("\nFEHLER: Datei '" + file.getName() + "' ist keine JSON-Datei\n\n");
				continue;
			}

			System.out.println("\n" + zaehler + ". Urteil\n");
			System.out.println("Datei: '" + file.getName() + "'");

			// ---Datei auslesen---
			Urteil urteil = new Urteil();

			urteil = urteil.dateiAuslesen(file); 

			// ---Zu Index pushen---
			push(urteil, file);
			zaehler++;
		}
	}

	/**
	 * Erstellt eine Verbindung zu den Elastic Servern und fügt das Urteil mittels urteilEinfuegen in den Index ein.
	 * @param urteil JSON Datei aus Ordner
	 * @param file Aktuelle Datei, nur für Errormeldung
	 */
	private void push(Urteil urteil, File file) {
		Connection connection = new Connection();
		Elastic_Operations elasticOperation = new Elastic_Operations();

		Connection.makeConnection();
		
		if (urteil.getAktenzeichen() != null) {

			System.out.println("Urteil einfügen...");
			Elastic_Operations.urteilEinfuegen(urteil);
			System.out.println("Urteil eingefügt --> " + urteil.getAktenzeichen() + "€" + urteil.getSchadenshoehe());
			System.out.println("---");

			try {
				Connection.closeConnection();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.err.println("FEHLER: Kein Aktenzeichen gefunden in " + file.getName());
		}
	}
}
