package de.uk.smartsentencing.urteil;

import java.io.File;

public class Urteil {

	private boolean bewaehrung;
	private int tagessaetze;
	private int monate;
	private String gericht;
	private float schadenshoehe;
	private boolean gestaendnis;
	private boolean vorstrafen;
	private String aktenzeichen;
	private String datum;
	private String urteilUrl;

	
	/**
	 * Liest als Input eine JSON-Datei eines Urteils aus und wendet die enthaltenen
	 * Werte auf eine neu erzeugte Klasse Urteil an.
	 * 
	 * @param file Das Urteil in Form einer JSON-Datei
	 * @return Neue Urteilsklasse mit den Werten aus der JSON-Datei
	 */
	public Urteil dateiAuslesen(File file) {

		DirectoryToStringBuilder directoryToStringBuilder = new DirectoryToStringBuilder();
		StringBuilder sb;

		StringBuilderToJsonObject stringBuilderToJsonObject = new StringBuilderToJsonObject(
				sb = directoryToStringBuilder.jsonToString(directoryToStringBuilder.jsonLesen(file))
				);
		
		return stringBuilderToJsonObject.fieldMapper();
	}

	// ---Getter und Setter---
	
	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}
	
	public boolean isBewaehrung() {
		return bewaehrung;
	}

	public void setBewaehrung(boolean bewaehrung) {
		this.bewaehrung = bewaehrung;
	}

	public int getStrafmass_tagessaetze() {
		return tagessaetze;
	}

	public void setStrafmass_tagessaetze(int strafmass_tagessaetze) {
		this.tagessaetze = strafmass_tagessaetze;
	}

	public int getStrafmass_monate() {
		return monate;
	}

	public void setStrafmass_monate(int strafmass_monate) {
		this.monate = strafmass_monate;
	}

	public String getGericht() {
		return gericht;
	}

	public void setGericht(String gericht) {
		this.gericht = gericht;
	}

	public float getSchadenshoehe() {
		return schadenshoehe;
	}

	public void setSchadenshoehe(float f) {
		this.schadenshoehe = f;
	}

	public boolean isGestaendnis() {
		return gestaendnis;
	}

	public void setGestaendnis(boolean gestaendnis) {
		this.gestaendnis = gestaendnis;
	}

	public boolean isVorstrafen() {
		return vorstrafen;
	}

	public void setVorstrafen(boolean vorstrafen) {
		this.vorstrafen = vorstrafen;
	}

	public String getAktenzeichen() {
		return aktenzeichen;
	}

	public void setAktenzeichen(String aktenzeichen) {
		this.aktenzeichen = aktenzeichen;
	}

	public int getTagessaetze() {
		return tagessaetze;
	}

	public void setTagessaetze(int tagessaetze) {
		this.tagessaetze = tagessaetze;
	}

	public int getMonate() {
		return monate;
	}

	public void setMonate(int monate) {
		this.monate = monate;
	}

	public String getUrl() {
		return urteilUrl;
	}

	public void setUrl(String url) {
		this.urteilUrl = url;
	}
	
	

}
