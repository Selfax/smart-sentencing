package de.uk.smartsentencing.urteil;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class DirectoryToStringBuilder {

	File file;
	FileInputStream fis=null;
	BufferedInputStream bis;

	/**
	 * Datei mittels InputStream einlesen.
	 * @param file JSON oder Text Datei
	 * @return Datei als Buffered Input Stream 
	 */
	public BufferedInputStream jsonLesen(File file) {

		try {
			fis = new FileInputStream(file);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		return bis = new BufferedInputStream(fis);
	}
	
	/**
	 * Erzeugt aus einem Buffered Input Stream einen StringBuilder.
	 * @param bis Buffered Input Stream 
	 * @return StringBuilder, der den Inputstream enthält
	 */
	public StringBuilder jsonToString(BufferedInputStream bis) {
		
		int c = 0;
		StringBuilder sb = new StringBuilder();

		try {
			while ((c = bis.read()) != -1) {
					sb.append((char) c);
			}
		} catch (IOException e) {
		}
		
		return sb;
		
	}

}
