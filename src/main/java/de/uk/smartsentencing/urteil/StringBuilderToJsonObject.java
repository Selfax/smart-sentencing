package de.uk.smartsentencing.urteil;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class StringBuilderToJsonObject {

	JSONObject urteilJson = new JSONObject();
	String urteilString;
	StringBuilder stringBuilder;
	Urteil urteilClass = new Urteil(); 
	
	public StringBuilderToJsonObject(StringBuilder sb) {
		this.stringBuilder = sb;
	}
	
	/**
	 * 
	 * @return
	 */
	public Urteil fieldMapper() {
		
		formStringBuilderToJsonObjects();
		map();
		return urteilClass; //neues Urteil mit Werten aus der JSON
	}
	
	/**
	 * Nimmt den vom JSONLeser übergebenen StringBuilder und speichert ihn als JSON Objekt. 
	 */
	public void formStringBuilderToJsonObjects() {
		
		try {
			urteilJson = new JSONObject(stringBuilder.toString()); //StringBuilder als JSON Objekt anlegen
			
			JSONObject urteilTemp = new JSONObject(urteilJson.toString()); //Untergeordnete JSON Objekte auch als Objekt anlegen
			urteilString= urteilTemp.toString();  //Wieder zu String konvertieren

		} catch (JSONException e1) {
			System.out.println("--JSONException--");
			System.out.println("Mögliche Fehler: Falsches JSON, falsche Typweitergabe (Object zu Array etc.)");
		}
	}
	
	/**
	 * Übergibt die Variablenwerte aus dem ausgelesenen JSON an ein Urteil Objekt. Bei unbekannten Variablen wird nicht abgebrochen.
	 */
	public void map() {
		ObjectMapper objectMapper = new ObjectMapper();
		
		try {
			 
			objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			
			urteilString = urteilString.replaceAll("Ã¶", "oe");
			urteilString = urteilString.replaceAll("Ã¤", "ae");
			
			urteilClass = objectMapper.readValue(urteilString, Urteil.class); //Mapping der Variablen von Test mit JSONString

			System.out.println("JSON: " + urteilString); //TEST
			
		} catch (JsonParseException e) {
			e.printStackTrace();
			System.out.println("Parsing Excpetion");
		} catch (JsonMappingException e) {
			e.printStackTrace();
			System.out.println("Mapping Exception");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("IOException");
		}
	}
}
