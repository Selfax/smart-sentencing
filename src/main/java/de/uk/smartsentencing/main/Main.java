package de.uk.smartsentencing.main;

import java.io.File;

import de.uk.smartsentencing.elastic_operations.Suchanfrage;
import de.uk.smartsentencing.htmlparser.HtmlToJson;
import de.uk.smartsentencing.urteil.JsonObjectToIndex;
import de.uk.smartsentencing.springtest.*;
import de.uk.smartsentencing.htmlparser.HtmlToJson;
import de.uk.smartsentencing.urteil.JsonObjectToIndex;

public class Main {

	public static void main(String[] args) throws Exception {

		// (I) 
		System.out.println("(I) HTML Auslesen");
			// ---HTML auslesen und zu JSON formatieren---

		String url = new String("https://www.justiz.nrw.de/nrwe/ag_koeln/j2016/648_Ds_43_16_Urteil_20160224.html"); // TEST URL anpassen
		HtmlToJson html = new HtmlToJson(url);
		System.out.println("URL: " + url);
		html.webZugriff(url);

		System.out.println("---");
		
		// (II) 
		/*
		System.out.println("(II) Indexierung");
			// ---JSON Urteil zu Elasticsearch pushen---
		
		File folder = new File("E:\\\\Workspaces\\\\eclipse-workspace\\\\smart-sentencing\\\\JSON_Urteile"); // Dateipfad anpassen
		//C:\Users\Pro\eclipse-workspace\Urteile\JSON_Urteile
		//E:\\Workspaces\\eclipse-workspace\\smart-sentencing\\JSON_Urteile
		
		JsonObjectToIndex dirLeser = new JsonObjectToIndex(folder);
		System.out.println("---");
		*/
		// (III) 
		System.out.println("(III) Suchanfrage");
			// ---In Index nach Urteilen suchen---
		
		System.out.println("Suchanfragen werden über das Webinterface gestellt.");
		
	}
}
