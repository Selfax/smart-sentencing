# Dokumentation Smart-Sentencing

### Inhalt
1. Projektidee, Konzept, Umsetzung
2. Anforderungen
3. Komponenten
4. Programmstruktur und Ausführung
5. Bekannte Beschränkungen
6. Weiterentwicklung
7. Ressourcen

---

1. **Projektidee, Konzept, Umsetzung**

Das Projekt ist die Umsetzung des Konzepts einer vergleichenden Strafzumessungs-Datenbank, das durch das Legal Tech Lab Cologne unter Anleitung von Prof. Dr. Dr. Frauke Rostalski ins Leben gerufen wurde. Es wurde im Rahmen des Hauptseminars *Verarbeitung mehrdimensionaler Daten* des Masterstudiengangs Informationsverarbeitung, geleitet von Prof. Dr. Øyvind Eide, an der Universität zu Köln entwickelt.

Zum Hintergrund:
Im Strafrecht werden Straftaten gemessen an ihrer Schwere und unter Berücksichtigung der Umstände mit Strafzumessungen belegt, die sich in ihrem Ausmaß von Bundesland zu Bundesland, oder bereits im selben Bundesland von Gericht zu Gericht unterscheiden können. Die Unterschiede zwischen den Strafzumessungen ist ein politisch und wissenschaftlich kontroverses Thema im Strafrecht und stellt daher eine relevante Ansatzstelle für weitere Untersuchungen dar. Die Zusammensetzung einer Strafzumessung wird in einem schriftlichen Urteil nachvollzogen, die im Falle der hier behandelten Urteilen als HTML Dokumente auf der Webseite des Justizministeriums NRW vorlagen.

Dieses Projekt zielt auf die automatische Auslese dieser Urteile und das Hochladen der relevanten Eckdaten auf eine Datenbank, um diese nach bestimmten inhaltlichen Aspekten durchsuchen zu können. Das Projekt soll damit einen Beitrag zum Diskurs über die Vergleichbarkeit von Strafurteilen leisten.

[**Auszug aus einem Musterurteil**](https://www.justiz.nrw.de/nrwe/ag_koeln/j2016/539_Ds_228_16_Urteil_20160706.html):
```html
<p class="absatzLinks"><strong>II.</strong></p>
<p class="absatzLinks">Die Hauptverhandlung hat zu folgenden Feststellungen geführt:</p>
<span class="absatzRechts">9</span>
<p class="absatzLinks">In der Nacht vom 31.12.2015 auf den 01.01.2016 zog der Angeklagte zwischen 23.15 Uhr und 23.45 Uhr dem auf dem Treppenaufgang zwischen Bahnhofsvorplatz und Dom in Köln befindlichen Geschädigten T.T. ein Mobiltelefon der Marke Samsung S6 aus der Jackentasche, um dieses für sich zu behalten. Das Mobiltelefon hatte der Geschädigte im November 2015 zu einem Neupreis von 599,00 € erworben.</p>
```
Zur Umsetzung des Projektes wurde eine Programmstruktur auf Basis von Java (SE 12) gewählt, die sich verschiedener vorgefertigter Ressourcen bedient, um die Schnittstelle zwischen Auslese, Indexierung und Suche darstellen zu können.

2. **Anforderungen**

Unter Absprache mit der juristischen Fachkenntnis des Legal Tech Labs wurden einige zentrale Aspekte der Urteile besprochen, die einen Großteil der Zusammensetzung einer Strafzumessung ausmachen.

Diese sind:
* *Formales*: Aktenzeichen, Gericht, Datum
* Tatbestand (etwa Diebstahl oder Betäubungsmitteldelikt)
* Schadenshöhe (in Euro oder als Sachschäden)
* Strafmaß (in Tagessätzen für Geldstrafen oder in Monaten für Haftstrafen)
* Vorhandensein eines Geständnisses
* Vorhandensein von Vorstrafen

*Weitere Aspekte wie gemeinschaftliche Tatbegehung oder erhöhte kriminelle Energie sind weitere Aspekte, die in der Analyse berücksichtigt werden könnten, im Basisprogramm aber zunächst ausgeklammert wurden.*

Die Auslese ist damit primär darauf gerichtet, den genannten Attributen eines Urteils die richtigen Werte zuzuordnen. Diese lassen sich leicht in Form von Strings, Integern und Booleschen Variablen in Datentypen umwandeln.
Die Urteile haben als HTML Dokumente eine definierte Form, die allerdings lediglich den Text formal strukturiert und wenig Verbindung zum Inhalt der Absätze enthält. Das Orten der für die Strafzumessung relevanten Informationen ist damit nur durch systematisches Durchsuchen des Volltexts möglich. Hierfür ergaben sich im Rahmen des Projektes zwei Möglichkeiten:
1. Menschliche Erkennung des Auftretens der relevanten Informationen und zielgerichtetes Auslesen dieser Informationen mittels einer Heuristik und regulärer Ausdrücke
2. Maschinelle Erkennnung mittels *Natural Language Processing* (NLP) - Trainieren der maschinellen Erkennung durch menschliches Tracking der Dokumente

Das Projekt wurde anhand des ersten Ansatzes umgesetzt, da die Ressourcen einer finanziellen, zeitlichen und infrastrukturellen Begrenzung unterlagen. Die Ergebnisse trugen hierbei viel zur Einschätzung der Prozesse und Vorbedingungen einer solchen Auslese bei. Ein größer angelegtes Projekt unter Verwendung von künstlicher Intelligenz und umfassender Kapazitäten ist bereits als Kooperation mit einem großen Forschungsinstitut als Fortentwicklung dieser Keimzelle angelegt.

3. **Komponenten**

Das Projekt besteht aus den folgenden drei Hauptkomponenten:
**Auslese** (Java, Regex-Ausdrücke, JSON Libraries) 
**Indexierung** (Elastic Stack) 
**Zugriff** (UI, Spring Webanwendung)

1. **Auslese**

**Pakete** 
```
de.uk.smartsentencing.main
de.uk.smartsentencing.urteil
de.uk.smartsentencing.htmlparser
```

Das Ziel der Auslese ist das Extrahieren der Kerninformationen aus dem HTML Volltext und die Umwandlung in ein maschinenlesbares JSON Format. 
Dazu wird zunächst der HTML Volltext per URL-API abgerufen und gespeichert. Aus dem Titel (der immer die Form "Gericht"/"Aktenzeichen" hat) und dem Tenor des Urteils können bereits Gericht, Datum, Aktenzeichen und das insgesamte Strafmaß ausgelesen werden. Eine eigene Klasse findet dann Paragraph II. des Urteils, in dem üblicherweise die Einzeltaten mit Daten und Schadenshöhen beschrieben sind, und liest diese ebenfalls aus. Die Beschreibung einer Einzeltat im Paragaph II. hat meist folgende Form:

>Am 21.06.2011 entwendete der Angeklagte gegen 14:45 Uhr aus den Auslagen der Firma T, L-Platz 1 – 5, F, 3 Musik-CDs zum Gesamtverkaufspreis von 28,97 Euro, indem er diese CDs in seinen Jutebeutel einsteckte und ohne zu zahlen den Kassenraum passierte. Er wurde nach Verlassen des Kassenbereichs durch den Zeugen xxx. gestellt. 
[Komplettes Urteil](https://www.justiz.nrw.de/nrwe/lgs/essen/ag_essen_steele/j2012/18_Cs_85_Js_755_11_390_11_Urteil_20120228.html)

Anschließend werden die ausgelesenen Informationen in ein Objekt zusammengefasst und im Ordner *JSON_Urteile* mit dem Dateinamen *"Aktenzeichen"€"Schadenshöhe".json* zwischengespeichert. Der Dateiname enthält dabei die Schadenshöhe, da im Programm bereits die Ansätze für die Auslese eines Urteils mit mehreren Einzeltaten angelegt sind &ndash; bei der Indexierung der Urteile auf Elasticsearch wird jedem Dokument eine eindeutige ID zugewiesen, die hier als das Aktenzeichen gesetzt ist. Da in einem Urteil mehrere Einzeltaten mit demselben Aktenzeichen ausgelesen werden können, wird der Name um die Schadenshöhe der Tat ergänzt.  

```Java
public JSONObject urteilToJson(Urteil e, JsonToDirectory jtd) throws JSONException, IOException {

			JSONObject json = new JSONObject();
			JSONObject urteil = new JSONObject();

			urteil = mapToJson(urteil, "aktenzeichen", e.getAktenzeichen());
			urteil = mapToJson(urteil, "gericht", e.getGericht());
			urteil = mapToJson(urteil, "datum", e.getDatum());
			urteil = mapToJson(urteil, "schadenshöhe", e.getSchadenshoehe());
			urteil = mapToJson(urteil, "tagessätze", e.getStrafmass_tagessaetze());
			urteil = mapToJson(urteil, "monate", e.getStrafmass_monate());
			urteil = mapToJson(urteil, "geständnis", e.isGestaendnis());
			urteil = mapToJson(urteil, "vorstrafen", e.isVorstrafen());

			json.put("urteil", urteil);
			System.out.println("---->JSON: " + urteil.toString()); 
			
			jtd.jsonToFile(urteil);
			
			return urteil;
	}
```

In der Anwendung erfolgen diese Schritte nach dem Einsetzen der entsprechenden Urteils-URL in der **Main** Klasse und dem Ausführen automatisch. **Hierbei können zurzeit nur URLs des Justizministeriums NRW ausgelesen werden, da das Programm auf die Struktur dieser Urteile vorangepasst ist.** Alle relevanten Schritte der Auslese werden in der Konsole der Entwicklungsumgebung mitgeloggt.
Weitere Beschränkungen werden folgend aufgelistet.

Nach der Auslese aller nötigen Urteile befindet sich im oben genannten Ordner der lokale Index.

2. **Indexierung**

**Pakete**
```
de.uk.smartsentencing.main
de.uk.smartsentencing.urteil
de.uk.smartsentencing.elastic_operations
```

Zum Indexieren der Dokumente auf Elasticsearch muss eine laufende Instanz eines Elastic Servers bestehen und eine Verbindung zum Server hergestellt werden. Für das Aufbauen der Verbindung werden in der Klasse **Connection** die Standardports und die Kommunikation mittels REST Client festgelegt. 

```Java
    static final String HOST = "localhost";
	static final int PORT_ONE = 9200;
	static final int PORT_TWO = 9201;
	static final String SCHEME = "http";

	static final String INDEX = "urteile";
```
So kann nach Import der **Connection** Klasse durch die Erzeugung eines Connection Objekts und dem Aufruf der Methode *makeConnection()* eine Verbindung zu Elasticsearch (lokal) hergestellt werden.
```Java
public static synchronized RestHighLevelClient makeConnection() {
		if (restHighLevelClient == null) {
				restHighLevelClient = new RestHighLevelClient(
						RestClient.builder(
						new HttpHost(HOST, PORT_ONE, SCHEME), 
						new HttpHost(HOST, PORT_TWO, SCHEME)));
			}
			return restHighLevelClient;
		}
```
Sobald die Verbindung aufgebaut ist, werden die im Ordner *JSON_Urteile* enthaltenen Dokumente ausgelesen und eine HashMap erstellt, auf die die Werte der JSON Datei gemappt werden. Die erstellte Map wird dann auf Elasticsearch hochgeladen. 
```Java
public static void urteilEinfuegen(Urteil urteil) {
		Map<String, Object> dataMap = new HashMap<String, 
		Object>(); // Mapping erstellen

		// ---Eigenschaften der Instanz auf Hashmap abbilden---

		dataMap.put("aktenzeichen", urteil.getAktenzeichen());
		dataMap.put("datum", urteil.getDatum());
		dataMap.put("gericht", urteil.getGericht());
		dataMap.put("schadenshoehe", urteil.getSchadenshoehe());
		dataMap.put("monate", urteil.getStrafmass_monate());
		dataMap.put("tagessaetze", urteil.getStrafmass_tagessaetze());
		dataMap.put("bewaehrung", urteil.isBewaehrung());
		dataMap.put("gestaendnis", urteil.isGestaendnis());
		dataMap.put("vorstrafen", urteil.isVorstrafen());

		IndexRequest indexRequest = new IndexRequest(INDEX).source(dataMap);

		try {
			IndexResponse response = restHighLevelClient.index(
					indexRequest.id(urteil.getAktenzeichen() + "€" + urteil.getSchadenshoehe()), // Aktenzeichen als ID																		// setzen
					COMMON_OPTIONS);
		} catch (ElasticsearchException e) {
			e.getDetailedMessage();
		} catch (java.io.IOException e) {
			e.getLocalizedMessage();
		}
	}
```
Sobald alle Dokumente im Elasticsearch Index enthalten sind, kann über verschiedene Methoden auf sie zugegriffen werden. Für die Entwicklung ist die einfachste Variante der Zugriff mit Kibana, ebenfalls ein Tool von Elastic, das nur mittels CLI gestartet werden muss und dann als grafische Oberfläche einer schon laufenden Elasticsearch Instanz fungiert. Zugriff direkt über das CLI oder über die Java API sind ebenfalls möglich. 

3. ##### **Zugriff**

Für das UI des Nutzers bestanden drei Möglichkeiten:
1. Eine ausführbare, lokale Version des Programms, die über eine interne UI bedienbare wäre
2. Ein Web Interface, mit dem der Nutzer auf einen Server zugreifen kann, auf dem alle notwendigen Komponenten installiert sind &ndash; also als Webdienst
3. Eine für den Nutzer umgebaute Variante der Kibana UI, in der Grafiken und Statistiken bereits vorhanden sind, deren Suchmöglichkeiten aber beschränkt sind

Es wurde sich dabei für Variante 2 entschieden, da dies dem späteren Nutzungsfall am besten entspricht und für den Nutzer den besten Zugang bedeutet.

**Pakete** 
```
de.uk.smartsentencing.springtest
de.uk.smartsentencing.elastic_operations
src/main/resources: templates & static
```
Für den Zugriff auf Elasticsearch wurde eine Webseite erstellt, mit der eine Suchanfrage auf den Server abgeschickt werden kann und die verschiedene Filteroptionen ermöglicht. Die Werkzeuge sind dabei zurzeit noch begrenzt, eine Volltextsuche ist etwa noch nicht möglich, diese lassen sich aber einfach erweitern. Die Website und der Zugriff auf Elasticsearch sind damit vorerst ein Proof of Concept.

Mit Ausführen der Klasse **Application** wird die Instanz eines Spring Servers gestartet, auf der die im Templates Ordner enthaltenen HTML Seiten *index.html* und *resultIndex.html* betrieben werden können (Zugriff per "localhost:8080/index"). Die Startseite  bietet einige Optionen zum Einstellen der Suche &ndash; bei Druck auf "Suchen" wird eine Anfrage mit den eingegebenen Parametern an die laufende Instanz von Elasticsearch geschickt, die Antwort wird schließlich auf *resultIndex.html* neben einer Auflistung der Suchparameter im Feld "Ergebnis" ausgegeben. Das Abfragen der Informationen aus der Elasticsearch-Antwort funktioniert wiederum als Auslese eines JSON Strings, dessen Werte an das für die Webanwendung erzeugte Urteils-Objekt (hier heißt es richtig Index-Objekt) übergeben und schließlich nach einer kurzen Formatierung mittels **Thymeleaf** auf die Ergebnisseite geladen werden. 
```Java
@PostMapping("/index")
    public String formSubmit(@ModelAttribute Index index) throws IOException {
    	System.out.println("====\n-->Neue Suchanfrage um " + tmstmp.getTimestamp() + "\n");
    	
    	Suchanfrage suche = new Suchanfrage(index);
    	String result = suche.suchanfrage().toString();
    	
    	System.out.println("\nInput von Webcontroller: " + result);
    	ResponseFormatter formatter = new ResponseFormatter(result);
    	System.out.println("Output aus Formatter: ");
    	formatter.format(index);
    	
        return "resultIndex";
    }
```
Ausschnitt des HTML Codes:
```html
<body>
	<h1>Result</h1>
    <p th:text="'Tatbestand: ' + ${index.tatbestand}"/>
    <p th:text="'Schadenshöhe: ' + ${index.schadenshoeheMod} + ' ' + ${index.schadenshoehe} + ' €'"/>
    <p th:text="'Strafmass: ' + ${index.strafmassModNumeric} + ' ' + ${index.strafmass} + ' ' + ${index.strafmassMod}"/>
    <p th:text="'Vorstrafen: ' + ${index.vorstrafen}" />
    <p th:text="'Gestaendnis: ' + ${index.gestaendnis}" />
    
    <p th:text="'Ergebnis: ' + ${index.result}" />
    <a href="/index">Submit another message</a>
</body>
</html>
```
Auf der Ergebnisseite ist schließlich die Antwort von Elastic und damit der Output des Gesamtsystems zu sehen.

4. ##### Programmstruktur und Ausführung



1.**Programmstruktur**

Die Hauptkomponenten des Programms sind zum Großteil in eigenen Paketen geordnet und mit Namen versehen, die ihren Funktionen entsprechen. Alle Methoden haben zudem eine Dokumentation, womit die genaue Nachverfolgung des Prozesses möglich sein sollte. 
Die wichtigsten Klassen des Programms sind dabei:
Für (I) Auslese:
- Urteil: Prototypisches Objekt für ein Urteil.
- HtmlToJson: Bündelt alle Methoden zur Auslese und Speicherung des HTML Volltextes.
- Paragraph_II_Leser: Findet und parst Paragraph II sowie Anzahl der Einzeltaten im Volltext.
- JsonToDirectory: Speichert Urteile in JSON Format in lokalem Ordner.

Für (II) Indexierung:
- Connection: Setzt Standardparameter für Verbindungsaufbau mit Elastic.
- JsonObjectToIndex & Elastic_Operations(urteilEinfuegen): Liest JSON Urteile aus lokalem Ordner aus, mappt die Werte auf eine HashMap und lädt sie zu Elasticsearch hoch.

Für (III) Zugriff:
- Application: Spring Server zum Hosten der Websites und der Anfragen.
- Index: Prototypisches Objekt für ein Urteil zur Übergabe an die Webanwendung.
- WebController: Sorgt für das Mapping der Suchanfrage an die entsprechende Klasse und formatiert die Antwort.
- Suchanfrage: Wandelt HTTP Suchanfrage in Elasticsearch Suchanfrage um und baut aus den eingegebenen Feldern eine Query.
- Index.html: Suchmaske.
- resultIndex.html: Ergebnis der Suchanfrage.

2. **Ausführung**

Die Anwendung ist zum jetzigen Zeitpunkt nur als Source Code in Verbindung mit einer lokal installierten Version von Elasticsearch möglich. 
Das reguläre Setup für Entwicklung am Projekt sieht wie folgt aus:

Java Source Code inklusive Spring, HTML, CSS und JS, sowie Elasticsearch Schnittstellen per Git ([SSH](git@gitlab.com:Selfax/smart-sentencing.git) oder [HTTPS](https://gitlab.com/Selfax/smart-sentencing.git
)) in einen beliebigen Ordner importieren. 
[Elasticsearch und Kibana](https://www.elastic.co/de/downloads/elasticsearch) herunterladen und in einen Ordner speichern &ndash; beide Programme sind per CLI leicht aus ihrem Bin-Ordner heraus auszuführen.
Das Projekt besitzt eine Maven-Einbindung, wodurch nach Einfügen der unten gelisteten Dependencies alle nötigen Plugins automatisch heruntergeladen und installiert werden. 

Sobald alles installiert ist, kann eine Instanz von Elasticsearch gestartet (Überprüfen, ob Server läuft mit **localhost:9200**) werden. Anschließend sollte eine Urteils-URL und der Dateipfad zum "JSON_Urteile" Ordner in der **Main** Klasse eingefügt werden. Dann kann **Main** ausgeführt werden. Gleichzeitig kann die **Application** Klasse der Spring Anwendung gestartet werden, sowie, falls für Testzwecke notwendig, eine Instanz von Kibana. Mittels Kibana (**localhost:5601**) kann der bereits hochgeladene Index unter dem Menüpunkt "Entwicklertools" durchsucht werden (Befehle dafür unter Dokumente -> Kibana Commands), die Standardsuche passiert allerdings über das Formular in der Webanwendung (**localhost:8080/index**).

5. ##### Bekannte Beschränkungen

Die Funktion der Anwendung ist zum jetzigen Zeitpunkt noch stark beschränkt und das Programm ein Prototyp. Beispielsweise können, wie oben angedeutet, keine Urteile eingelesen werden, die mehr als einen Angeklagten behandeln, deren Schadenshöhe reine Sachschäden sind, oder die auch nur die Einzeltaten in einem anderen Paragraphen als Paragraph II beschreiben. Die Heuristik ist damit relativ unflexibel, allerdings wäre es mit einem größeren Zeitumfang möglich, sie entsprechend anzupassen und ihr mehr Stabilität zu verleihen. Ähnlich verhält es sich mit dem Zugriff auf die Datenbank über eine Webanwendung und die Installation eines Servers, der die Anwendung als Webdienst anbieten könnte &ndash; auch hier wären ein größerer Zeitumfang und personelle Kapazität einfache Lösungen, für die aktuellen Beschränkungen.

Folgend eine Auflistung der bekannten Beschränkungen:
- Auslese:
    - Kein Einlesen möglich von: Sachschäden, gemeinschaftlichen Straftaten, Gewichtung der Vorstrafen, Bewährungszeit, Jugendstraftaten
    - Struktur des Urteils muss dem eines Musterurteils entsprechen ([Beispiel](https://www.justiz.nrw.de/nrwe/ag_koeln/j2016/535_Ds_26_16_Urteil_20160401.html))
    - Auslese ist fehleranfällig, eine menschliche Überprüfung der Urteile wäre zurzeit notwendig
- Zugriff:
    - Filteroptionen sind beschränkt
    - Volltextsuche noch nicht möglich
    - Im Ergebnis kann nur ein gefundenes Urteil angezeigt werden
    - Zurzeit nur als Entwickler mit allen notwendigen Komponenten möglich

6. ##### Weiterentwicklung

Die Weiterführung des Projekts entscheidet sich unter Absprache mit dem Legal Tech Lab unter Berücksichtigung möglicher Kooperationsangebote mit anderen Forschungseinrichtungen.

7. ##### Ressourcen
##### Java
Word to int Parser:
https://stackoverflow.com/questions/26948858/converting-words-to-numbers-in-java
Elasticsearch:
https://www.elastic.co/guide/en/elasticsearch/client/java-api/current/index.html
Timestamp:
https://www.mkyong.com/java/how-to-get-current-timestamps-in-java/
##### Webentwicklung
Searchbar: 
https://www.w3schools.com/howto/howto_css_searchbar.asp
ScrollTable: 
https://stackoverflow.com/questions/30295872/html-layout-with-only-part-of-the-page-scrolling
https://stackoverflow.com/questions/17773938/add-a-list-item-through-javascript
Tooltip:
https://www.w3schools.com/css/css_tooltip.asp
Springboot:
https://www.baeldung.com/spring-boot-start
https://spring.io/guides/gs/spring-boot/ 
https://spring.io/guides/gs/serving-web-content/
> Did you notice that there wasn’t a single line of XML? No web.xml file either. This web application is 100% pure Java and you didn’t have to deal with configuring any plumbing or infrastructure.

Bean
>In its JavaBeans application program interface for writing a component, Sun Microsystems calls a component a "Bean" (thus continuing their coffee analogy). A Bean is simply the Sun Microsystems variation on the idea of a component.
In object-oriented programming and distributed object technology, a component is a reusable program building block that can be combined with other components in the same or other computers in a distributed network to form an application. Examples of a component include: a single button in a graphical user interface, a small interest calculator, an interface to a database manager. Components can be deployed on different servers in a network and communicate with each other for needed services. A component runs within a context called a container. Examples of containers include pages on a Web site, Web browsers, and word processors.
Standardpfade für SpringBoot:

HTML: `[Projektname]/src/main/resources/templates (nur mit Thymeleaf)/site.html`
CSS, JS: `[Projektname]/src/main/resources/static/siteStyle.css|siteScript.js`
z.B.: `WebApp/src/main/resources/templates/greeting.html`

Maven
https://www.mkyong.com/maven/how-to-install-maven-in-windows/
Maven installieren, Maven Variablen zu den Umgebungsvariablen des Computers hinzufügen, anschließend ist das CLI von Maven verfügbar, z.B.:
mvn dependency:tree 
gibt alle installierten Plugins an

### Dependencies 
###### (SpringBoot, SpringBoot Dev Tools, ThymeLeaf, Java Version auf 1.8 festgelegt):

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>net.javaguides.maven-web-project</groupId>
	<artifactId>Urteile1</artifactId>
	<packaging>war</packaging>
	<version>0.0.1-SNAPSHOT</version>
	<name>Urteile1 Maven Webapp</name>
	<url>http://maven.apache.org</url>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.1.6.RELEASE</version>
		<relativePath />
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<dependencies>

		<dependency>
			<groupId>org.elasticsearch</groupId>
			<artifactId>elasticsearch</artifactId>
			<version>7.1.0</version>
		</dependency>

		<dependency>
			<groupId>org.elasticsearch.client</groupId>
			<artifactId>elasticsearch-rest-high-level-client</artifactId>
			<version>7.1.0</version>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<version>2.9.9</version>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-core</artifactId>
			<version>2.9.9</version>
		</dependency>

		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>3.8.1</version>
			<scope>test</scope>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.json/json -->
		<dependency>
			<groupId>org.json</groupId>
			<artifactId>json</artifactId>
			<version>20180813</version>
		</dependency>


		<!-- <dependency> <groupId>org.apache.logging.log4j</groupId> <artifactId>log4j-core</artifactId> 
			<version>2.8.2</version> </dependency> <dependency> <groupId>org.apache.logging.log4j</groupId> 
			<artifactId>log4j-to-slf4j</artifactId> <version>2.8.2</version> </dependency> 
			<dependency> <groupId>org.slf4j</groupId> <artifactId>slf4j-api</artifactId> 
			<version>1.7.25</version> </dependency> <dependency> <groupId>org.slf4j</groupId> 
			<artifactId>slf4j-jdk14</artifactId> <version>1.7.25</version> </dependency> -->

		<dependency>
			<groupId>com.google.code.gson</groupId>
			<artifactId>gson</artifactId>
			<version>2.8.5</version>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-core</artifactId>
			<version>2.9.9</version>
		</dependency>

		<dependency>
			<groupId>org.jsoup</groupId>
			<artifactId>jsoup</artifactId>
			<version>1.11.3</version>
		</dependency>

		<!-- Springboot -->
		<!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-dependencies -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-log4j2</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
		</dependency>

		<!-- 	<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-devtools</artifactId>
			</dependency>
 -->

		<!-- Thymeleaf -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-thymeleaf</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<scope>test</scope>
		</dependency>


	</dependencies>

	<build>
		<finalName>Urteile1</finalName>
	</build>
</project>

```

