**Ausführung**

Die Anwendung ist zum jetzigen Zeitpunkt nur als Source Code in Verbindung mit einer lokal installierten Version von Elasticsearch möglich. 
Das reguläre Setup für Entwicklung am Projekt sieht wie folgt aus:

Java Source Code inklusive Spring, HTML, CSS und JS, sowie Elasticsearch Schnittstellen per Git (SSH oder HTTPS) in einen beliebigen Ordner importieren.
[Elasticsearch und Kibana](https://www.elastic.co/de/downloads/elasticsearch) herunterladen und in einen Ordner speichern &ndash; beide Programme sind per CLI leicht aus ihrem Bin-Ordner heraus auszuführen.
Das Projekt besitzt eine Maven-Einbindung, wodurch automatisch alle nötigen Plugins heruntergeladen und installiert werden. 

Sobald alles installiert ist, kann eine Instanz von Elasticsearch gestartet werden (Überprüfen, ob Server läuft mit **localhost:9200**). Anschließend sollte eine Urteils-URL (eine URL zu einem Urteil des Justizministeriums NRW aus der Urteilstabelle: [Beispiel](https://www.justiz.nrw.de/nrwe/ag_koeln/j2016/535_Ds_26_16_Urteil_20160401.html)) und der Dateipfad zum "JSON_Urteile" Ordner in der **Main** Klasse eingefügt werden (damit der Dateipfad auf den Ordner zeigt, in dem die Urteile lokal liegen). Dann kann **Main** ausgeführt werden. Gleichzeitig kann die **Application** Klasse der Spring Anwendung gestartet werden, sowie, falls für Testzwecke notwendig, eine Instanz von Kibana. Mittels Kibana (**localhost:5601**) kann der bereits hochgeladene Index unter dem Menüpunkt "Entwicklertools" durchsucht werden (Befehle dafür unter Dokumente -> Kibana Commands), die reguläre Suchfunktion ist das Formular in der Webanwendung, das aufrufbar ist, sobald der Spring-Server gestartet wurde (**localhost:8080/index**).

Die vollständige Doku findet sich [hier](https://gitlab.com/Selfax/smart-sentencing/blob/master/Dokumente/Dokumentation.md).